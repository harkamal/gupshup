defmodule Gupshup.Room do
  use Gupshup.Web, :model

  schema "rooms" do
    field :name, :string
    field :description, :string
    field :img_name, :string

    timestamps
  end

  @required_fields ~w(name description)
  @optional_fields ~w(img_name)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
