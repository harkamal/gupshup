defmodule Gupshup.ChatController do
  use Gupshup.Web, :controller
  alias Gupshup.User
  alias Gupshup.Room

   def index(conn, %{"name" => name}) do
    IO.inspect(name)
    user_session = verify_user(conn)
    room = Repo.get_by!(Room, name: name)

    unless user_session do
      {:ok, user} = create_user(conn)
      conn = put_session(conn, :user_id, user.id)
      user_session = get_session(conn, :user_id)
      IO.puts inspect(user_session)
    end
    render conn, "lobby.html", user_session: user_session, room: room
  end

  def index(conn, _params) do
    user_session = verify_user(conn)

    unless user_session do
      {:ok, user} = create_user(conn)
      conn = put_session(conn, :user_id, user.id)
      user_session = get_session(conn, :user_id)
      IO.puts inspect(user_session)
    end
    render conn, "lobby.html", user_session: user_session
  end

  def create_user(conn) do
    user = User.changeset(%User{}, %{name: "annonymous"})
    case user.valid? do
      true ->
        Repo.insert(user)
      errors ->
        IO.puts "something wrong....................."
    end
  end

  def verify_user(conn) do
    user_session = get_session(conn, :user_id)
    if user_session && Repo.get(User, user_session) do
      user_session
    else
      nil
    end
  end
end
