defmodule Gupshup.PageController do
  use Gupshup.Web, :controller
  alias Gupshup.Room

  def index(conn, _params) do
    rooms = Repo.all(Room)
    render(conn, "index.html", rooms: rooms)
  end
end
