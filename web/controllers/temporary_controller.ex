defmodule Gupshup.TemporaryController do
  use Gupshup.Web, :controller

  alias Gupshup.Temporary

   def index(conn, _params) do
   	render(conn, "index.html")
   end
end