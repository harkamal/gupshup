defmodule Gupshup.RoomChannel do
  use Gupshup.Web, :channel
  alias Gupshup.User
  alias Gupshup.Room
  alias Gupshup.ChannelMonitor

  def join("rooms:lobby", message, socket) do
    Process.flag(:trap_exit, true)

    current_user = Repo.get!(User, message["user_session"])
    socket = assign(socket, :current_user, current_user)

    socket = assign(socket, :room, 'lobby')

    users = ChannelMonitor.user_joined("rooms:lobby", current_user)["rooms:lobby"]
    
    send(self, {:after_join, message, users})
    
    {:ok, socket}
  end

  def join("rooms:" <> room_name, message, socket) do

    room = Repo.get_by!(Room, name: room_name)
    Process.flag(:trap_exit, true)

    current_user = Repo.get!(User, message["user_session"])

    socket = assign(socket, :current_user, current_user)

    socket = assign(socket, :room, room.name)

    users = ChannelMonitor.user_joined("rooms:#{room_name}", current_user)["rooms:#{room_name}"]

    send(self, {:after_join, message, users})

    {:ok, socket}

    # {:error, %{reason: "can't do this"}}
  end


  def handle_info({:after_join, msg, users}, socket) do
    lobby_update(socket, users)

    user = Repo.get!(User, msg["user_session"])
    broadcast! socket, "user:entered", %{user: user.name, user_session: user.id}
    push socket, "join", %{status: "connected"}
    {:noreply, socket}
  end


  def terminate(_reason, socket) do
    user = socket.assigns.current_user
    room = socket.assigns.room
    users = ChannelMonitor.user_left("rooms:#{room}", user.id)["rooms:#{room}"]
    lobby_update(socket, users)
    broadcast! socket, "user:terminated", %{user: user.name}
    :ok
  end

  def handle_in("new:msg", msg, socket) do
    user = Repo.get!(User, msg["user_session"])
    
    if user.name != msg["user"] do
      {:ok, user} = update_user(user, msg["user"])
      socket = assign(socket, :current_user, user)
    end
    
    broadcast! socket, "new:msg", %{user: user.name, body: msg["body"], user_session: user.id}
    {:reply, {:ok, %{msg: msg["body"]}}, assign(socket, :user, user.name)}
  end

  def update_user(user, name) do
    changeset = User.changeset(user, %{name: name})
    Repo.update(changeset)
  end

  defp lobby_update(socket, users) do
    broadcast! socket, "lobby_update", %{ users: get_usernames_and_ids(users) }
  end

  defp get_usernames_and_ids(nil), do: []
  
  defp get_usernames_and_ids(users) do
    Enum.map users, &([&1.id, &1.name])
  end

end