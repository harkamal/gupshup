class Gossip { 
  static init(socket){
    console.log("initi")
    console.log("Join" + window.roomName)
    var roomName = (typeof window.roomName === 'undefined') ? 'lobby' : window.roomName
    var $status = $("#status")
    var $messages = $("#messages")
    var $users_list = $("#users-list")
    var $input = $("#message-input")
    var $send = $("#sendMessage")
    var $username = $("#username_" + window.userSession)
    socket.onError( ev => console.log("ERROR", ev) )
    socket.onClose( e => console.log("CLOSE", e))

    var chan = socket.channel("rooms:"+ roomName, {user_session: window.userSession})
    chan.join()
      .receive("ignore", () => console.log("auth error"))
      .receive("ok", () => {
        console.log("join ok")
      }) 
      .receive("timeout", () => console.log("Connection interruption"))
      
    chan.onError(e => console.log("something went wrong", e))
    chan.onClose(e => console.log("channel closed", e))
    
    $input.off("keypress").on("keypress", e => {
      if (e.keyCode == 13 && $input.val().length > 0 ) {
        chan.push("new:msg", {user: $username.val(), body: $input.val(), user_session: window.userSession}, 10000)
        $input.val("")
        scroll_position();
      }
    })
    
    $send.click(function(){
      if ($input.val().length > 0){
        chan.push("new:msg", {user: $username.val(), body: $(".emojionearea-editor").html(), user_session: window.userSession}, 10000)
        $input.val("")
        $(".emojionearea-editor").text("");
        scroll_position();
      }
    })    

    function scroll_position(){
      var scrollDiv = $('#messages');
      console.log(scrollDiv[0].scrollHeight)
      var height = scrollDiv[0].scrollHeight;
      console.log(height)
      var intervalId = setInterval(function() {
        // Give up and scroll anyway
        clearInterval(intervalId)
        scrollDiv.animate({ scrollTop: height })
      }, 10);
    }

    $username.off("keydown").on("keydown", e => {
      console.log($("#user_" + window.userSession).text())
      $("#user_" + window.userSession).text(($username.val()))
    })

    chan.on("new:msg", msg => {
      var old_name = $("#user_" + msg["user_session"]).text()
      // remove []
      if ( old_name != msg["user"]) {
        $messages.append(`<p><b>${old_name}</b> renamed to <b>${msg["user"]}</b></p>`)  
      }

      $("#user_" + msg["user_session"]).text(msg["user"])
      $messages.append(this.messageTemplate(msg))
      scroll_position();
    })

    chan.on("user:entered", msg => { 
      var username = this.sanitize(msg.user || "anonymous")
      $("#username_" + msg["user_session"]).val(username)
      $messages.append(`<br/><i>[${username} Joined]</i>`)
      scroll_position();

    })

    chan.on('lobby_update', function(response) {
      console.log(JSON.stringify(response.users));
      $users_list.html("")
      $("#onlineCount").html(response.users.length)
      $.each(response.users, function(index, id_name){
        var user_id = "user_" + id_name[0]
        $users_list.append(`<a href='#' id=${user_id}>${id_name[1]}</a><br/>`)   
      })
    });

    chan.on("user:terminated", msg => {
      $messages.append(`<br/><i>${msg.user} left</i>`) 
      scroll_position();
    })
  } 
  
  static sanitize(html){ 
    return $("<div/>").text(html).html() 
  }
  
  static messageTemplate(msg){
    let username = this.sanitize(msg.user || "anonymous")
    let body = msg.body
    return(`<p><a href='#'>[${username}]</a>&nbsp; ${body}</p>`)
  }
}
export default Gossip
