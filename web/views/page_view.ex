defmodule Gupshup.PageView do
  use Gupshup.Web, :view

  def display_room_name(room_name) do
    String.capitalize(room_name)
  end
end
