defmodule Gupshup.Router do
  use Gupshup.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end


  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Gupshup do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/chat", ChatController, :index
    get "/chat/:name", ChatController, :index

    get "/ipl-cricket", TemporaryController, :index

    resources "/rooms", RoomController
  end

  # Other scopes may use custom stacks.
  # scope "/api", Gupshup do
  #   pipe_through :api
  # end
end
