defmodule Gupshup.Repo.Migrations.AddFieldsToRooms do
  use Ecto.Migration

  def change do
    alter table(:rooms) do
      add :description, :string
      add :img_name, :string
    end
  end
end
