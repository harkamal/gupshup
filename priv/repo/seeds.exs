# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Gupshup.Repo.insert!(%Gupshup.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Gupshup.Repo
alias Gupshup.Room

Repo.insert! %Room{name: "Dating", description: "Chat, make friends, have fun with your first date.", img_name: "dating.jpg"}


Repo.insert! %Room{name: "Fun", description: "Felling alone? Make friends, have Fun. Enjoy Chatting!!", img_name: "fun.jpg"}

Repo.insert! %Room{name: "Sport", description: "Talk about your favorite sport. Enjoy Chatting!!", img_name: "sport.jpg"}


Repo.insert! %Room{name: "Yoga", description: "Talk about Yoga. Enjoy Chatting!!", img_name: "yoga.jpg"}


Repo.insert! %Room{name: "Education", description: "Talk about education. Enjoy Chatting!!", img_name: "education.jpg"}


Repo.insert! %Room{name: "Betting", description: "Talk about betting. Enjoy Chatting!!", img_name: "betting.jpg"}


Repo.insert! %Room{name: "Jobs", description: "Are you looking for a job?. Find someone here to help you out.", img_name: "job.jpg"}
